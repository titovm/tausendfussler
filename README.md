# Tausendfüßler

Tausendfüßler is a dark ambient project, oosing from the swamps of Saint-Petersburg (Russia). Static dark energy below the city vibrates on specific frequencies, which makes the music in sync with the city.

The artist behind the project is **Mark Titov** - musician, programmer and psychotherapist.
