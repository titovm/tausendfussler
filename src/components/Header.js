import React from 'react'
import PropTypes from 'prop-types'

/* eslint-disable */

const Header = (props) => (
    <header id="header" style={props.timeout ? {display: 'none'} : {}}>
        <div className="logo">
            <span className="icon fa-microchip"></span>
        </div>
        <div className="content">
            <div className="inner">
                <h1>Tausendfüßler</h1>
                <p>Dark ambient music coming from Saint-Petersburg, Russia.</p>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('albums')}}>Albums</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('singles')}}>Singles</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('about')}}>About</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('contact')}}>Contact</a></li>
            </ul>
        </nav>
    </header>
)

Header.propTypes = {
    onOpenArticle: PropTypes.func,
    timeout: PropTypes.bool
}

export default Header
