import React from 'react'
import PropTypes from 'prop-types'

import pic01 from '../images/pic01.jpg'
import pic03 from '../images/pic03.jpg'

/* eslint-disable */

class Main extends React.Component {
  render() {

    let close = <div className="close" onClick={() => {this.props.onCloseArticle()}}></div>

    return (
      <div ref={this.props.setWrapperRef} id="main" style={this.props.timeout ? {display: 'flex'} : {display: 'none'}}>

        <article id="albums" className={`${this.props.article === 'albums' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h2 className="major">Albums</h2>
          <span className="image main"><img src={pic01} alt="" /></span>
          <iframe width="100%" src="https://bandcamp.com/EmbeddedPlayer/album=793264338/size=large/bgcol=333333/linkcol=e99708/artwork=small/transparent=true/" seamless><a href="http://kalpamantra.bandcamp.com/album/in-der-blauen-w-ste">In Der Blauen Wüste by Tausendfüßer</a></iframe>
          <p>This album was released in the april of 2018 on british Kalpamantra dark ambient label.</p>
          <p>The concept was of a septopod, walking down the blue desert, while giving birth.</p>
          {close}
        </article>

        <article id="singles" className={`${this.props.article === 'singles' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h2 className="major">Singles</h2>
          <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/711005265&color=%23ff9900&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
          {close}
        </article>

        <article id="about" className={`${this.props.article === 'about' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h2 className="major">About</h2>
          <span className="image main"><img src={pic03} alt="" /></span>
          <p>Tausendfüßler is a dark ambient project, oosing from the swamps of Saint-Petersburg (Russia). Static dark energy below the city vibrates on specific frequencies, which makes the music in sync with the city.</p>
          <p>The artist behind the project is <strong><a href="https://www.discogs.com/artist/2430548-Марк-Титов" target="_blank" title="see Discogs profile">Mark Titov</a></strong> - musician, programmer and psychotherapist.</p>
          {close}
        </article>

        <article id="contact" className={`${this.props.article === 'contact' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h2 className="major">Contact</h2>
          <form method="post" action="#">
            <div className="field half first">
              <label htmlFor="name">Name</label>
              <input type="text" name="name" id="name" />
            </div>
            <div className="field half">
              <label htmlFor="email">Email</label>
              <input type="text" name="email" id="email" />
            </div>
            <div className="field">
              <label htmlFor="message">Message</label>
              <textarea name="message" id="message" rows="4"></textarea>
            </div>
            <ul className="actions">
              <li><input type="submit" value="Send Message" className="special" /></li>
              <li><input type="reset" value="Reset" /></li>
            </ul>
          </form>
          <ul className="icons">
            {/*<li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>*/}
            <li><a href="https://www.youtube.com/channel/UCW74_F51eVtxHMAJBwnQ9xA" target="_blank" className="icon fa-youtube"><span className="label">YouTube</span></a></li>
            <li><a href="https://www.facebook.com/pg/Tausendf%C3%BC%C3%9Fler-226740734739748/" target="_blank" className="icon fa-facebook"><span className="label">Facebook</span></a></li>

          </ul>
          {close}
        </article>

      </div>
    )
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
}

export default Main
